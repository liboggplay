#!/usr/bin/env python

"""Setup script for the PyAnnodex module distribution."""

import os
import re
import sys
import string

from distutils.core import setup
from distutils.extension import Extension

oggplay_include_dir = "../include"
oggplay_lib_dir = "../src/liboggplay/.libs"
oggplay_libs = ["oggplay"]


_oggplaymodule = Extension(
    name='_oggplay',
    sources=['liboggplay_wrap.c'],
    include_dirs=[oggplay_include_dir],
    library_dirs=[oggplay_lib_dir],
    libraries=oggplay_libs)

setup (
    name = "pyoggplay",
    version = "1",
    description = "Python bindings for oggplay libraries",
    author = "Shane Stephens",
    author_email = "shane.stephens@gmail.com",
    platforms = ["POSIX", "MacOS", "Windows"],
    license = "BSD",
    packages = ["oggplay"],
    package_dir = {"oggplay" : "."},
    ext_package = "oggplay",
    ext_modules = [_oggplaymodule],
    classifiers = ["Development Status :: 4 - Beta",
                   "Intended Audience :: Developers",
                   "Operating System :: OS Independent",
                   "Programming Language :: C",
                   "Programming Language :: Python",
                   "Topic :: Multimedia :: Video",
                   "Topic :: Software Development :: Libraries :: Python Modules"]
    )
