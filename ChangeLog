2009-08-17  Viktor Gal <viktor.gal at maeth dot com>

	* Version 0.2.0 
	
	* Fix for freeing uninitialized pointer in oggplay_seek
	  (ticket: 429)
	* Fix semaphore handling on FreeBSD, Solaris and AIX.
	* Unify the return value of semaphore handling functions
	  as they does not necessarily mean the same on various
	  OSes, e.g. Unix-like systems vs Micrsoft
	* Check for EOF when reading initial Ogg Data
	  (ticket: 435)
	* Added new function oggplay_get_duration that returns
	  the length of the Ogg content.
	* Re-factored the YUV to RGB conversion functions.
          Changed the API of the conversion functions in order to better
	  reflect what the functions will really do:
  	    - oggplay_yuv2bgr -> oggplay_yuv2bgra
	    - oggplay_yuv2rgb -> oggplay_yuv2rgba
            - oggplay_yuv2argb for supporting ARGB packaging.
	  
	  - Fixed the vanilla implementation of the conversion functions.
	  - Added SSE2, SSE, MMX and AltiVec implementations of the 
	    conversion functions.
	  - Added run-time CPU-extension detection.
	  - Added support for for YUV 4:2:2 and 4:4:4 pixel format support.
	  - Fixed the conversion functions for odd sized frames.
	* Added libtiger support
	* Mozilla bug 482461: Fix the presentation time for oggz-chopped
	  content. 
	* Mozilla bug 480014: Added integer overflow checks + input argument 
	  validations in different functions. Two new error codes has been 
	  introduced in OggPlayErrorCode:
	    E_OGGPLAY_TYPE_OVERFLOW: integer overflow detected.
	    E_OGGPLAY_OUT_OF_MEMORY: ran out of memory.
	* Removed the use of the deprecated 'fish_sound_set_interleave' function.
	* Fixed file closing in oggplay_file_reader_destroy: only close the 
	  file handle if it was successfully opened.
	* Mozilla bug 480063: stop the loop for requesting more chunks 
	  if we received a shutdown event in the meanwhile. 
	  Patch by Chris Double.
	* Mozilla bug 492436: a track with UNKNOWN content type shall not be 
	  ever active, as we do not know what codec shall be used. 
	  Added dummy support for Dirac codec in 'callbacks' struct in order 
	  to prevent out-of-bound access to the array.
	  A new error code has been introduced E_OGGPLAY_TRACK_IS_UNKNOWN
	* Fix for liboggplay not correctly handling Theora files with offsets.
	  Patch by Gregory Maxwell <greg at xiph.org>
	* Mozilla bug 493224 & 463726: enforce Ogg's requirement that
	  granule position cannot decrease in any part of the stream.
	* Added cpu.c, ppc/oggplay_yuv2rgb_altivec.c, x86/oggplay_yuv2rgb_x86.c,
	  x86/yuv2rgb_x86.h to noinst_HEADERS in order to create a 
	  proper archive of the current release.	
	* Added error checking for data callback function
	  If the OggPlayDataCallback is not set for the OggPlay handle
	  return with E_OGGPLAY_UNINITIALISED when calling oggplay_step_decoding
	  as there is no function that would process the decoded frames. Thus
 	  there's no point in decoding.
	* Added solution files for Visual Studio 2008
	* Revisited the initialisation of the Ogg streams.
	  The oggplay_initialise function will try to read the headers of each 
	  streams before returning, in order to avoid to be able to activate
	  streams with bogus headers.
	  The oggplay_set_track_active will return E_OGGPLAY_TRACK_UNINITIALISED
	  error code if the given stream was not properly initialised.
	  Changed the behaviour of the various callback functions (OggzReadPacket),
	  made them more liberal: if an error occures don't stop the whole
	  decoding of the Ogg content, but skip the packet, and hope that
	  there some valid packets of the streams are still about to come.
	  And in worst case scenario simply disable the given bogus stream, i.e.
	  stop decoding the rest of it.
	* Mozilla bug 496051: Fix presentation time for theora streams.
	  From theora bitstream version 3.2.1 onwards, frame granule numbers
	  are relative to the end of the frame, i.e. frame granule numbers
	  start at 1. The presentation time is calculated by
	  (frame_number * granule_period), which is only correct if the 
	  frame number starts at 0 (theora bitstream versions older than 3.2.1)
	  credits for Chris Pearce.
	* Update the minimum required version of liboggz to 0.9.8
	* Update the minimum required version of libfishsound to 0.9.1
	* Updated shared version info to 2:0:1
	
2008-08-05  Silvia Pfeiffer <silvia at annodex dot net>

	* Preparing for first release 0.0.1
